<?php
 if (isset($_POST['submit'])){
    include "db_connection.php"
    $req = $bdd->prepare("insert into album(images) values(?)");
    $req->execute(array($_FILES["file"]["name"],$_FILES["file"]["size"], 
    $_FILES["file"]["type"],file_get_contents($_FILES["file"]["tmp_name"])));
    
    }
   $file = $_FILES['file'];

   $fileName = $_FILES['file']['name'];
   $fileTmpName = $_FILES['file']['tmp_name'];
   $fileSize = $_FILES['file']['size'];
   $fileError = $_FILES['file']['error'];
   $fileType = $_FILES['file']['type'];

   $fileExt = explode('.', $fileName);
   $fileActualExt = strtolower(end($fileExt));

   $allowed = array('jpg', 'jpeg', 'png', 'pdf');

   if (in_array($fileActualExt, $allowed)){
    if ($fileError === 0){
        if ($fileSize < 1000000){
           $fileNameNew = uniqid('', true).".".$fileActualExt;
           $fileDestination = 'uploads/'.$fileNameNew;
           move_uploaded_file($fileTmpName, $fileDestination);
           header("location : index.php?uploadsuccess");
        } else {
            echo "Votre fichier est trop volumineux !";
        }
    } else {
        echo "Une erreur est survenue lors du chargement de votre fichier.";
    }
   } else {
       echo "Vous ne pouvez pas télécharger ce type de fichier.";
 }
?>