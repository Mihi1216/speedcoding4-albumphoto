<?php

    function debug($var, $die = false) {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if ($die) die;
    }

    $host = 'localhost';
    $dbname = 'mon_album';
    $username = 'root';
    $password = '';
    
    $dsn = "mysql:host=$host;dbname=$dbname";
    $sql = "SELECT * FROM `album` ORDER BY `id` DESC";

    try{
        $pdo = new PDO($dsn, $username, $password);
        $stmt = $pdo->query($sql);
    
        if($stmt === false){
         die("Erreur");
        }
    
      }catch (PDOException $e){
         echo $e->getMessage();
      }

?>