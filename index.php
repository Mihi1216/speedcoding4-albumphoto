<?php
 include "db_connection.php";
 $req=$pdo->prepare("select * from images where id=? ");
 $req->setFetchMode(PDO::FETCH_ASSOC);  
 $tab=$req->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Album Photo</title>
</head>
<body>
   <div class="container">
        <h1>Mon Album Photo</h1>
        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <input type="file" name="file">
            <button type="submit" name="submit">Télécharger</button>
        </form>
        <img src="<?php echo $tab['images']; ?>" />
   </div>
</body>
</html>
